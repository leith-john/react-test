require("coffee-script/register");
configure = require("./leitjohn-webpack-starter/index.coffee").configure;

config = configure({
  host: "localhost",
  production: false
});

module.exports = config;
