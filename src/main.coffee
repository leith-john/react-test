$ = require("jquery")
bv = require("backbone_views")
CommentBoxView = require("./views/test.cjsx")

# Application = require("leitjohn-webpack-starter/src/application")


class Main extends CommentBoxView
  collection: bv.BaseModel.collection()
  el: "#app"

  initialize: (options) ->
    super(options)
    @collection.set([
      {author: "Pete Hunt", text: "This is one comment"},
      {author: "Jordan Walke", text: "This is *another* comment"}
    ])


# window.Main = Main

window.Main = Main
